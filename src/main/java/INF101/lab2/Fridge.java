package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    private List<FridgeItem> itemList = new ArrayList<FridgeItem>();

    @Override
    public int nItemsInFridge() {
        return itemList.size();
    }

    @Override
    public int totalSize() {
        return 20;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < totalSize()) {
            itemList.add(item);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (itemList.contains(item)) {
            itemList.remove(item);
        } else {
            throw new NoSuchElementException("Fridge does not contain item");
        }
    }

    @Override
    public void emptyFridge() {
        itemList.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredItemsList = new ArrayList<FridgeItem>();
        for (FridgeItem item : itemList) {
            if (item.hasExpired()) {
                expiredItemsList.add(item);
            }
        }
        itemList.removeAll(expiredItemsList);
        return expiredItemsList;
    }
}
